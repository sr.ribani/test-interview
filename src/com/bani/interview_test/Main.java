package com.bani.interview_test;

import java.util.Arrays;

public class Main {

    int x;
    int[] nums;
    String word;

    /***
     * We have array of integers called nums, write a function to return all numbers (in a form
     * of array of integers) that when subtracted by any of integers in nums doesn't return
     * number that is < 0
     */
    private int[] getSubtractedBy(int... args) {
        return Arrays.stream(args).filter(x1 ->
                Arrays.stream(args).noneMatch(x2 -> x1 - x2 < 0)
        ).toArray();
    }

    /***
     * We have array of integers called nums and an integer called x, write a function to return
     * all numbers (in a form of array of integers) that when divided by any of integers in nums
     * doesn't return x
     */
    private int[] getDividedByX(int... args) {
        return Arrays.stream(args).filter(x1 ->
                Arrays.stream(args).noneMatch(x2 -> x1/x2 == x)
        ).toArray();
    }

    /***
     * We have a string called word and an integer called x, write a function to return an array
     * of strings containing all strings that has length x.
     */
    private String[] getLengthByX() {
        String[] strArray = word.split(" ");
        return Arrays.stream(strArray).filter(s -> s.length() == x).toArray(String[]::new);
    }

    public static void main(String[] args) {
	    Main main = new Main();
	    main.nums = main.getSubtractedBy(3, 1, 4, 2);
        System.out.println("Result 1 : "+ Arrays.toString(main.nums));

	    main.x = 4;
	    main.nums = main.getDividedByX(1, 2, 3, 4);
        System.out.println("Result 2 : "+ Arrays.toString(main.nums));

	    main.x = 3;
	    main.word = "I'm Jhon Mayer Yeah";
	    String[] result = main.getLengthByX();
        System.out.println("Result 2 : "+ Arrays.toString(result));
    }
}
